package com.example.braapp


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var vista: View = inflater.inflate(R.layout.fragment_home, container, false)
        vista.letra.setOnClickListener {
            startActivity(Intent(activity, MenuJuego::class.java))
        }
        return vista
    }


}
