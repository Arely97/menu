package com.example.braapp


import android.annotation.SuppressLint

import android.content.Intent

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View

import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity

import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_ayuda.*
import kotlinx.android.synthetic.main.activity_ayuda.view.*
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    @SuppressLint("WrongConstant")
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnAyuda.setOnClickListener {
            val bottomSheet = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
            val view = LayoutInflater.from(this).inflate(R.layout.activity_ayuda, contenedor)
            view.dato.text =
                "Estamos en Mantenimiento solo funciona el iniciar sin conexión"
            bottomSheet.setContentView(view)
            bottomSheet.show()

        }
        google.setOnClickListener {
            Toast.makeText(this, "Area en mantenimiento no funcional", Toast.LENGTH_SHORT).show()
        }
        btnlogin.setOnClickListener {
            Toast.makeText(this, "Area en mantenimiento no funcional", Toast.LENGTH_SHORT).show()
        }
        txtForgotPassword.setOnClickListener {
            Toast.makeText(this, "Area en mantenimiento no funcional", Toast.LENGTH_SHORT).show()
        }
        create2.setOnClickListener {
            Toast.makeText(this, "Area en mantenimiento no funcional", Toast.LENGTH_SHORT).show()
        }

        face.setOnClickListener {
            Toast.makeText(this, "Area en mantenimiento no funcional", Toast.LENGTH_SHORT).show()
        }
        conexion.setOnClickListener {
            val intent = Intent(this, PopUpWindow::class.java)
            intent.putExtra("popuptitle", "Registro Sin Conexión")
            intent.putExtra("popupbtn", "Entrar")
            intent.putExtra("popuptext", "Avatar")
            intent.putExtra("popuptext1", "Nombre de Usuario:")
            intent.putExtra("darkstatusbar", false)
            startActivity(intent)
        }
    }
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }
}