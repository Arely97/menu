package com.example.braapp


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_menu_principal.*


class MenuPrincipal : AppCompatActivity() {

    private var username = ""
    private var sex = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_principal)
        val bundle = intent.extras
        username = bundle?.getString("username", "usuario") ?: ""
        sex = bundle?.getString("clave", "numero") ?: ""
        if (sex.toInt() == 0) {
            user.setBackgroundResource(R.drawable.nina)
        } else if (sex.toInt() == 1) {
            user.setBackgroundResource(R.drawable.nino)
        } else if (sex.toInt() == 2) {
            user.setBackgroundResource(R.drawable.mujer)
        } else if (sex.toInt() == 3) {
            user.setBackgroundResource(R.drawable.hombre)
        } else if (sex.toInt() == 4) {
            user.setBackgroundResource(R.drawable.adulta)
        } else if (sex.toInt() == 5) {
            user.setBackgroundResource(R.drawable.adulto)
        } else {
            user.setBackgroundResource(R.drawable.sexo1)
        }
        juego.setOnClickListener {
            val intent = Intent(this, JuegosActivity::class.java)
            intent.putExtra("username", "$username")
            intent.putExtra("clave", "$sex")
            startActivity(intent)
        }
        traductor.setOnClickListener {
            Toast.makeText(this, "Area de traductor en mantenimiento", Toast.LENGTH_SHORT).show()
        }
        historia.setOnClickListener {
            Toast.makeText(this, "Area de historia en mantenimiento", Toast.LENGTH_SHORT).show()
        }
        Material.setOnClickListener {
            Toast.makeText(this, "Area de Material en mantenimiento", Toast.LENGTH_SHORT).show()
        }
        referencia.setOnClickListener {
            Toast.makeText(this, "Area de referencia en mantenimiento", Toast.LENGTH_SHORT).show()
        }
        ayuda.setOnClickListener {
            Toast.makeText(this, "Area de ayuda en mantenimiento", Toast.LENGTH_SHORT).show()
        }
       user.setOnClickListener {
            Toast.makeText(this, "Area de barra en mantenimiento", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    private fun hideSystemUI() {

        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE

                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }
}