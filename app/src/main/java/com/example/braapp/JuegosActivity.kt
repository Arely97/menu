package com.example.braapp

import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_menu_principal.*

class JuegosActivity : AppCompatActivity() {
    private var username = ""
    private var sex = ""
    private lateinit var homeBtn: ImageButton
    private lateinit var addBtn: ImageButton

    private lateinit var searchBtn: ImageButton


    private lateinit var mViewPager: ViewPager
    private lateinit var mPagerViewAdapter: PagerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_juegos)
        val bundle = intent.extras
        username = bundle?.getString("username", "usuario") ?: ""
        sex = bundle?.getString("clave", "numero") ?: ""
        when {
            sex.toInt() == 0 -> {
                user.setBackgroundResource(R.drawable.nina)
            }
            sex.toInt() == 1 -> {
                user.setBackgroundResource(R.drawable.nino)
            }
            sex.toInt() == 2 -> {
                user.setBackgroundResource(R.drawable.mujer)
            }
            sex.toInt() == 3 -> {
                user.setBackgroundResource(R.drawable.hombre)
            }
            sex.toInt() == 4 -> {
                user.setBackgroundResource(R.drawable.adulta)
            }
            sex.toInt() == 5 -> {
                user.setBackgroundResource(R.drawable.adulto)
            }
            else -> {
                user.setBackgroundResource(R.drawable.sexo1)
            }
        }

        // init views
        mViewPager = findViewById(R.id.mViewPager)
        homeBtn = findViewById(R.id.homeBtn)
        addBtn = findViewById(R.id.addBtn)

        searchBtn = findViewById(R.id.searchBtn)


        //onclick listner

        homeBtn.setOnClickListener {
            mViewPager.currentItem = 0

        }

        searchBtn.setOnClickListener {

            mViewPager.currentItem = 1

        }

        addBtn.setOnClickListener {
            mViewPager.currentItem = 2

        }






        mPagerViewAdapter = PagerViewAdapter(supportFragmentManager)
        mViewPager.adapter = mPagerViewAdapter
        mViewPager.offscreenPageLimit = 3



        mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                changeTabs(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })




        mViewPager.currentItem = 0
        homeBtn.setImageResource(R.drawable.memomo)


    }

    private fun changeTabs(position: Int) {


        if (position == 0) {
            homeBtn.setImageResource(R.drawable.memomo)
            searchBtn.setImageResource(R.drawable.tictocmo1)
            addBtn.setImageResource(R.drawable.ahorcadomo1)


        }
        if (position == 1) {
            homeBtn.setImageResource(R.drawable.memomo1)
            searchBtn.setImageResource(R.drawable.tictocmo)
            addBtn.setImageResource(R.drawable.ahorcadomo1)


        }
        if (position == 2) {
            homeBtn.setImageResource(R.drawable.memomo1)
            searchBtn.setImageResource(R.drawable.tictocmo1)
            addBtn.setImageResource(R.drawable.ahorcadomo)
        }

    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }
}